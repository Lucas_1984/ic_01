﻿namespace IC_01
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Convert = new System.Windows.Forms.Button();
            this.mphtokph = new System.Windows.Forms.Label();
            this.kphLabel = new System.Windows.Forms.Label();
            this.mphTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Convert
            // 
            this.Convert.Location = new System.Drawing.Point(75, 94);
            this.Convert.Name = "Convert";
            this.Convert.Size = new System.Drawing.Size(136, 59);
            this.Convert.TabIndex = 0;
            this.Convert.Text = "Convert Speed";
            this.Convert.UseVisualStyleBackColor = true;
            this.Convert.Click += new System.EventHandler(this.Convert_Click);
            // 
            // mphtokph
            // 
            this.mphtokph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mphtokph.Location = new System.Drawing.Point(154, 48);
            this.mphtokph.Name = "mphtokph";
            this.mphtokph.Size = new System.Drawing.Size(100, 21);
            this.mphtokph.TabIndex = 2;
            // 
            // kphLabel
            // 
            this.kphLabel.Location = new System.Drawing.Point(12, 51);
            this.kphLabel.Name = "kphLabel";
            this.kphLabel.Size = new System.Drawing.Size(131, 18);
            this.kphLabel.TabIndex = 4;
            this.kphLabel.Text = "Your Speed in KPH";
            this.kphLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.kphLabel.Click += new System.EventHandler(this.kphLabel_Click);
            // 
            // mphTextBox
            // 
            this.mphTextBox.Location = new System.Drawing.Point(154, 12);
            this.mphTextBox.Name = "mphTextBox";
            this.mphTextBox.Size = new System.Drawing.Size(100, 20);
            this.mphTextBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Enter Speed in MPH";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(291, 206);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mphTextBox);
            this.Controls.Add(this.kphLabel);
            this.Controls.Add(this.mphtokph);
            this.Controls.Add(this.Convert);
            this.Name = "Form1";
            this.Text = "Speed Conversion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Convert;
        private System.Windows.Forms.Label mphtokph;
        private System.Windows.Forms.Label kphLabel;
        private System.Windows.Forms.TextBox mphTextBox;
        private System.Windows.Forms.Label label1;
    }
}

