﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IC_01

    

{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void kphLabel_Click(object sender, EventArgs e)
        {

        }

        private void Convert_Click(object sender, EventArgs e)
        {
            try
            {
                //variables for mph and to calculate kph
                double mph;
                double tokph;

                // mph derived from user input
                mph = double.Parse(mphTextBox.Text);

                // conversion formula assigned to variable
                tokph = mph * 1.60394;
                
                mphtokph.Text = tokph.ToString("n3");
            }
            catch
            {
                // message that displays to enter numbers only //
                MessageBox.Show("Enter Numbers only.");
                    }
        }
    }
}
